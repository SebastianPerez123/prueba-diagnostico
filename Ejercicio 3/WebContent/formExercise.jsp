<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ejercicio 3</title>
</head>
<body>
	<header>
		<h1>Ejercicio 3</h1>
	</header>
	<section>
		<h1>Indique 1 para concurso importante y 0 para un concurso que no es importante, adicionalmente ingrese la suerte</h1>
		<form action="ejercicio3Controller" method="post">
			<% Integer concursos =(Integer) request.getAttribute("concursosPerdidos"); 
				for(int i = 0; i<concursos ; i++){
			%>
				<div>
					<label>Importancia del concurso numero: <%= i+1 %></label>
					<select name="importacia<%=i%>">
						<option value="0">0</option>
						<option value="1">1</option>
					</select>
				</div>
				<div>
					<label>Suerte que obtuvo en el concurso numero: <%= i+1 %></label>
					<input type="text" name="suerte<%=i%>">
				</div>
			<%} %>
			<% Integer todosConcursos = (Integer) request.getAttribute("concursosPerdidos"); 
				Integer numeroConcursos = (Integer) request.getAttribute("concursos"); %>
			<input type="hidden" name="concursosPerdidos" value="<%=todosConcursos%>">
			<input type="hidden" name="todosConcursos" value="<%=numeroConcursos%>">
			<input type="hidden" name="option" value="2">
			<button>Enviar</button>
		</form>
	</section>
</body>
</html>