<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Ejercicio 03</title>
</head>
<body>
	<header>
		<h1>Ejercicio 3</h1>
	</header>
	<section>
		<form action="ejercicio3Controller" method="post">
			<div>
				<label>Ingrese el numero de concursos que puede perder</label>
				<input type="text" name="numeroConcursos">
			</div>
			<div>
				<label>Total de concursos</label>
				<input type="text" name="todosConcursos">
			</div>
			<input type="hidden" name="option" value="1">
			<button>Enviar</button>
		</form>
	</section>
</body>
</html>