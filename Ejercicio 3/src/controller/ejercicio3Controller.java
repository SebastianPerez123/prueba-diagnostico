package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ejercicio3Controller
 */
@WebServlet("/ejercicio3Controller")
public class ejercicio3Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int option = Integer.parseInt(request.getParameter("option"));
		if(option == 1) {
			int concursos = Integer.parseInt(request.getParameter("numeroConcursos"));
			int concursosPerdidos = Integer.parseInt(request.getParameter("todosConcursos"));
			request.setAttribute("concursos", concursos);
			request.setAttribute("concursosPerdidos", concursosPerdidos);
			RequestDispatcher rs = request.getRequestDispatcher("/formExercise.jsp");
			rs.forward(request, response);
		}
		if(option == 2){
			int concursosPerdidos = Integer.parseInt(request.getParameter("concursosPerdidos"));
			int numeroConcursos = Integer.parseInt(request.getParameter("todosConcursos"));
			int todaSuerte = 0;
			int suerte = 0;
			int importancia = 0;
			ArrayList<Integer> competenciaImportante = new ArrayList<>();
			for(int i = 0;i <= numeroConcursos ; i++) {
				suerte = Integer.parseInt(request.getParameter("suerte"+i+""));
				todaSuerte = todaSuerte+suerte;
				importancia = Integer.parseInt(request.getParameter("importacia"+i+""));
				if(importancia == 1) {
					competenciaImportante.add(suerte);
				}
			}
			Collections.sort(competenciaImportante);
			int suertefinal = 0;
			int concurso = competenciaImportante.size()-numeroConcursos;
			for(int i = 0; i < concurso ; i++ ) {
				suertefinal = suertefinal+competenciaImportante.get(i);
			}
			int resultado = todaSuerte - (2*suertefinal);
			request.setAttribute("suerteFinal", resultado);
			RequestDispatcher rs = request.getRequestDispatcher("/finalPage.jsp");
			rs.forward(request, response);
		}
	}

}
