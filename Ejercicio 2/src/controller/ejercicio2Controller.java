package controller;
import java.util.*;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ejercicio2Controller
 */
@WebServlet("/ejercicio2Controller")
public class ejercicio2Controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rs = request.getRequestDispatcher("/finalPage.jsp");
		String palabra1 = request.getParameter("palabra1");
		String palabra2 = request.getParameter("palabra2");
		int count = 0;
		for(int i = 0; i < palabra1.length();i++) {
			char letra = palabra1.charAt(i);
			for(int q = 0; q < palabra2.length();q++) {
				char letra2 = palabra2.charAt(q);
				if(letra == letra2) {
					request.setAttribute("letra"+count+"", letra);
					System.out.println("letra"+count+"");
					count++;	
				}
			}
		}
		request.setAttribute("count", count);
		rs.forward(request, response);
	}

}
